package com.captain.cookingrecipe.model
import androidx.annotation.DrawableRes
data class Menu (
    val id: Int,
    @DrawableRes val imageResourceId: Int,
    val name: String,
    val cals: Int,
    val ingredient: String,
    val detail: String,
    )