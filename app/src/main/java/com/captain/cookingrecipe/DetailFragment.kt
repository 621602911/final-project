package com.captain.cookingrecipe

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.captain.cookingrecipe.databinding.FragmentDetailBinding
import com.captain.cookingrecipe.databinding.FragmentHomeBinding


class DetailFragment : Fragment() {
    private lateinit var menu: com.captain.cookingrecipe.model.Menu
    private val myDataset = com.captain.cookingrecipe.data.DataSource().getData()
    private var itemId = 0
    private val navigationArgs: DetailFragmentArgs by navArgs()
    private var _binding: FragmentDetailBinding?=null
    private val binding get() = _binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDetailBinding.inflate(inflater,container,false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // retrieve id
        itemId = navigationArgs.itemId

        //find id match with dataset
        myDataset.forEach {
            if (it.id == itemId) {
                menu = it
            }
        }
        binding.apply {
            name.text = menu.name
            cals.text = menu.cals.toString()
            ingredient.text = menu.ingredient
            detail.text = menu.detail
            menuImageDetail.setImageResource(menu.imageResourceId)
        }
    }
}

