package com.captain.cookingrecipe.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.captain.cookingrecipe.R
import com.captain.cookingrecipe.data.DataSource
import com.captain.cookingrecipe.model.Menu
import com.google.android.material.card.MaterialCardView

class ItemAdapter (
    private val context: Context?,
    private val data:List<Menu>,
    private val onItemClick:(Menu) -> Unit
): RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {
    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val menuImage: ImageView = view.findViewById<ImageView>(R.id.menu_image)
        val menuName: TextView = view.findViewById<TextView>(R.id.menu_name)
        val menuCals: TextView = view.findViewById<TextView>(R.id.menu_cals)
        val item: MaterialCardView = view.findViewById<MaterialCardView>(R.id.idMenu)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_item, parent, false)
        return ItemViewHolder(adapterLayout)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val menu = data[position]
        holder.menuName.text = menu.name
        holder.menuImage.setImageResource(menu.imageResourceId)
        holder.menuCals.text = menu.cals.toString()
        holder.item.setOnClickListener{
            onItemClick(menu)
        }
    }
}