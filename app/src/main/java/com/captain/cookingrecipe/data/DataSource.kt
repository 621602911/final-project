package com.captain.cookingrecipe.data


import com.captain.cookingrecipe.R
import com.captain.cookingrecipe.model.Menu

class DataSource {
    fun getData(): List<Menu> {
        return listOf(
            Menu(
                1,
                R.drawable.cesar,
                "Cesar salad",
                300,
                "วัตถุดิบ สำหรับ 1 จาน\nผักสลัด   ตามชอบ กรูตอง    ตามชอบ\n เบคอน     1 แถว\n มายองเนส        2 ช้อนโต๊ะ\n พาเมซานชีส      1 ช้อนโต๊ะ\n น้ำมันมะกอก     1/2 ช้อนโต๊ะ ",
                "วิธีทำ\n1.ล้างผักสลัดแล้วนำไปหั่นเป็นชิ้นพอดีคำ\n2.ใส่เบคอน และกรูตองเข้าไป\n3.จากน้ำผสมน้ำสลัดโดยใช้มายองเนส พาร์เมซานและน้ำมันมะกอก\n4.นำทุกอย่างมาคลุกเคล้ากัน พร้อมเสิร์ฟ "
            ),
            Menu(
                2,
                R.drawable.recipe,
                "Taco",
                250,
                "วัตถุดิบ\nแผ่นแป้ง Taco 3 แผ่น\nเนื้อวัว 200 กรัม\nกระเทียม 3 กลีบ\nหอมหัวใหญ่สับ 1/2 หัว\nมะเขือเทศสับ 1 ลูก\nพริกหวาน 1/2 ลูก\n" +
                        "ปาปริก้า 1 ช้อนโต๊ะ\nมายองเนสผสมซอสบาบีคิว 60 กรัม\nผักชี ตามชอบ",
                "วิธีทำ\n" +
                        "1.ตั้งกระทะไฟปานกลาง ใส่น้ำมัน ผัดกระเทียมและหอมหัวใหญ่จนสุก" +
                        "\n" +
                        "2.ใส่มะเขือเทศ ผักชี และพริกสามสี ผัดต่อจนสุก " +
                        "\n" +
                        "3.ผัดจนมะเขือเทศเริ่มเละ ใส่เนื้อวัวลงผัดพอสุก " +
                        "\n" +
                        "4. ปรุงรสด้วยมายองเนสผสมซอสบาบีคิวปาปริก้า และเกลือ ผัดให้เข้ากัน พักไว้" +
                        "\n" +
                        "5.นำแผ่นแป้ง Taco เข้าอบที่อุณหภูมิ 150 องศาเซลเซียส 5 นาที" +
                        "\n" +
                        "6.นำไส้ที่ผัดไว้ ใส่ลงแแป้งทาโก้ พร้อมเสิร์ฟ "
            ),
            Menu(
                3,
                R.drawable.steakkk,
                "ฺBeef Steak",
                430,
                "วัตถุดิบ\n" +
                        "เนื้อวากิวฮอกไกโดF1 ส่วนสตริปลอยน์ 350 กรัม\n" +
                        "เกลือ พริกไทย 1 ช้อนชา\n" +
                        "น้ำมันมะกอก 1 ช้อนโต๊ะ\n" +
                        "กระเทียม 1 หัว\n" +
                        "ไทม์ 1 ก้าน\n" +
                        "โรสแมรี่ 1 ก้าน\n" +
                        "บราวน์ซอส 1 ถ้วยตวง\n" +
                        "เนยเซนต์แคลร์ 2 ช้อนโต๊ะ\n" +
                        "เห็ดทรัฟเฟิลขูด สำหรับโรยหน้า\n",
                "วิธีทำ\n"+
                        "1.หั่นเอามันส่วนเกินจากเนื้อออก หั่นเป็นชิ้นเล็ก ๆ เตรียมไว้สำหรับเจียวเอาน้ำมัน" +
                        "หมักเนื้อส่วนที่เหลือด้วย เกลือรมควัน พริกไทยป่น และน้ำมันมะกอก นวดเบา ๆ ทั้งสองด้าน\n" +
                        "2.ตั้งกระทะเหล็กหล่อให้ร้อน ใส่มันเนื้อลงไปเจียวให้น้ำมันออก ใส่กระเทียมหั่นครึ่งลงไป พร้อมกับเนื้อ\n" +
                        "3.ย่างจนผิวนอกเริ่มสุก ใช้เวลาด้านละประมาณ 2นาทีครึ่ง จนเนื้อเริ่มขึ้นสี แล้วจึงแต่งกลิ่นด้วย ไทม์และโรสแมรี่ พร้อมกับตบเนยเซนต์แคลร์ลงไป 1 ช้อนโต๊ะ นำเนื้อสเต๊กขึ้นพัก ประมาณ 5 นาที"
            ),
            Menu(
                4,
                R.drawable.lemonsh,
                "Lemon shrimp",
                190,
                "วัตถุดิบ\n" +
                        "กุ้งขาว (แกะเปลือกผ่าหลัง)\n" +
                        "รากผักชีสับละเอียด 3 ช้อนโต๊ะ\n" +
                        "กระเทียมสับละเอียด 3 ช้อนโต๊ะ\n" +
                        "พริกขี้หนูสับละเอียด 2 ช้อนโต๊ะ\n" +
                        "น้ำมะนาว 3 ช้อนโต๊ะ\n" +
                        "น้ำปลา 3 ช้อนโต๊ะ\n" +
                        "น้ำตาลทราย 1 ช้อนชา\n" +
                        "ก้านคะน้าและแครอต แช่น้ำเย็นจัด",
                "วิธีทำ\n" +
                        "1. ลวกกุ้ง โดยตั้งน้ำให้เดือดจัด ใส่กุ้งลงไปลวก พอกุ้งเริ่มเปลี่ยนสี หรือประมาณ 5-10 วินาที ให้รีบตักขึ้นทันที จะสุกพอดีและกุ้งไม่หด (ในน้ำลวกกุ้งใส่ใบโหระพาไปนิดจะหอมมากค่ะ)\n" +
                        "2. ทำน้ำจิ้ม โดยนำน้ำมะนาว น้ำปลา และน้ำตาลทราย คนผสมให้น้ำตาลละลาย จากนั้นชิมรส ถ้าจัดเกินไปจะเติมน้ำซุปจากการลวกกุ้งนิดหน่อยก็ได้ค่ะ เมื่อได้รสที่ชอบแล้วใส่กระเทียม รากผักชี และพริกขี้หนูสับ ตามลงไป เสิร์ฟคู่กับก้านคะน้า และแครอตแช่เย็น"
            ),
            Menu(
                5,
                R.drawable.barbecue,
                "Pork Ribs with Cheese",
                390,
                "วัตถุดิบ\n" +
                        "ซี่โครงหมู 300 กรัม\n" +
                        "ซอสบาร์บีคิว 200 กรัม\n" +
                        "ชีส 100 กรัม\n" +
                        "ออริกาโน่แห้ง 2 ช้อน\n" +
                        "ใบไทม์แห้ง 2 ช้อนโต๊ะ\n" +
                        "น้ำสะอาด 250 มิลลิลิตร\n" +
                        "น้ำมัน สำหรับทอด ",
                "วิธีทำ\n" +
                        "1.นำน้ำมันลงหม้อหุงข้าวชาร์ปแคนดี้รอให้น้ำมันร้อน\n" +
                        "2.นำซี่โครงหมูลงทอดพอขึ้นสีทั้ง 4 ด้าน\n"+
                        "3.นำซอสบาร์บีคิวเทลงหม้อหุง\n" +
                        "4.ใส่ใบไทม์ และ ออริกาโน่ ลงไป\n" +
                        "5.เติมน้ำสะอาดให้พอท่วมซี่โครงหมู ตุ๋นประมาณ 40 นาที\n"+
                        "6.นำชีสใส่หม้อ แล้วปิดฝา รอละลายซักครู่\n" +
                        "7.เรียบร้อยแล้ว แค่นี้ก็พร้อมฟินกับ “ซี่โครงหมูบาร์บีคิวอบชีสยืด"
            ),
            Menu(
                6,
                R.drawable.salmonsteak,
                "Salmon Steak",
                390,
                "วัตถุดิบ \n" +
                        "ปลาแซลมอน\n" +
                        "พริกไทยดำบด\n" +
                        "เกลือป่น\n" +
                        "เนย\n" +
                        "เลมอนผ่าซีก\n" +
                        "ซอสขาวสำเร็จรูป\n" +
                        "มันบด\n" +
                        "บรอกโคลีลวก",
                "วิธีทำ \n" +
                        "1.โรยพริกไทยและเกลือป่นลงบนเนื้อปลาแซลมอน พักไว้สักครู่\n" +
                        "2.ใส่เนยลงไปในกระทะ พอเนยละลายใส่ปลาแซลมอนลงไป พอเนื้อปลาแซลมอนเริ่มสุกพลิกกลับด้าน จัดใส่จาน เสิร์ฟกับบรอกโคลี มันบด เลมอน และซอสขาว"
            ),
            Menu(
                7,
                R.drawable.spinach,
                "Spinach with Cheese",
                250,
                "วัตถุดิบ \n" +
                        "ผักโขม 7 ต้น\n" +
                        "หอมใหญ่ 2 หัว\n" +
                        "กระเทียม 1 หัว\n" +
                        "เนยสด ปริมาณตามชอบ (อยากได้รสชาติมัน ๆ ก็ใส่เยอะหน่อย)\n" +
                        "นมสด\n" +
                        "เกลือ\n" +
                        "แป้งมันสำปะหลัง\n" +
                        "ชีสขูด",
                "วิธีทำ \n" +
                        "1.ต้มผักโขมให้สุกแล้วสับเป็นชิ้นเล็ก ๆ\n" +
                        "2.สับหอมใหญ่และกระเทียม เตรียมไว้\n" +
                        "3.ใส่เนยลงกระทะ ผัดหอมใหญ่ก่อน พอหอมใหญ่ใกล้สุกใส่กระเทียม พอกระเทียมสุกตามด้วยผักโขม จากนั้นเทนมสดลงไปนิดหน่อย ปรุงรสด้วยเกลือ และทำให้ส่วนผสมเหนียวขึ้นมาหน่อยด้วยแป้งมันสำปะหลัง ผัดให้เข้ากัน\n" +
                        "4.ตักใส่ถ้วยขนาดพอเหมาะ โรยชีสลงไปตามชอบแล้วนำเข้าเตาอบ เอาออกมา พร้อมเสิร์ฟ"
            ),
            Menu(
                8,
                R.drawable.cocktail,
                "Shrimp Cocktail",
                120,
                "กุ้งขาวผ่าหลังดึงเส้นดำ 20 ตัว\n" +
                        "กระเทียมสับ 3 กลีบ\n" +
                        "เกลือ 1 ช้อนชา\n" +
                        "พริกไทย 1 ช้อนชา\n" +
                        "ซอสมะเขือเทศ 1 ถ้วย\n" +
                        "ซอสพริก 1 ถ้วย\n" +
                        "ซอสทาบาสโก้ 1 ช้อนโต๊ะ\n" +
                        "ซอสวูสเตอร์ 1 ช้อนโต๊ะ\n" +
                        "ปาปริกาป่น 1 ช้อนโต๊ะ\n" ,
                "วิธีทำ \n" +
                        "1.ตั้งกระทะ ใส่น้ำมันเล็กน้อย นำกระเทียมสับลงไปผัดให้หอม ตามด้วยกุ้งขาวผ่าหลังดึงเส้นดำ ผัดจนกุ้งสุกแล้วปรุงรสด้วยเกลือ และพริกไทย นำขึ้นพักไว้\n" +
                        "2.ผสมซอสโดยนำซอสมะเขือเทศ ซอสพริก ซอสทาบาสโก้ ซอสวูสเตอร์ และปาปริกาป่น เทลงในชามผสมคนให้เข้ากัน\n" +
                        "3.จัดเสิร์ฟในภาชนะเล็ก ๆ โดยเทซอสลงไปก่อน แล้ววางกุ้งที่ผัดแล้วตามลงไป ตกแต่งด้วยเลมอนฝานและผักชีลาว"
            ),
            Menu(
                9,
                R.drawable.mashedpotato,
                "Bacon-Cheddar Mashed Potatoes",
                260,
                "มันฝรั่งต้มสุก\n" +
                        "เนยจืด\n" +
                        "เกลือป่น\n" +
                        "พริกไทยป่น\n" +
                        "เบคอนทอด\n" +
                        "มอสซาเรลล่าชีส\n" +
                        "พาร์สลีย์ สับละเอียด" ,
                "วิธีทำ \n" +
                        "1.ใช้ส้อมยีมันฝรั่งต้มสุกกับเนยให้เข้ากัน โดยยีไม่ต้องให้เละมาก หรือตามชอบ\n" +
                        "2.ปรุงรสด้วยเกลือป่นและพริกไทยป่นเล็กน้อย โรยเบคอนทอดกรอบลงไป คนผสมให้เข้ากันดี โรยชีสลงไป\n" +
                        "3.นำไปอบที่อุณหภูมิ 180 องศาเซลเซียส นาน 20-25 นาที หรือจนชีสเหลืองสวยตามชอบ พออบเสร็จแล้วนำออกมาโรยพาร์สลีย์สับละเอียด เสิร์ฟขณะร้อน ๆ"
            ),
            Menu(
                10,
                R.drawable.sandwich,
                "Avocado Chicken Breast Sandwich",
                125,
                "วัตถุดิบ\n" +
                        "ขนมปังมัลติเกรน 2 แผ่น\n" +
                        "อโวคาโด 1 ผล\n" +
                        "อกไก่ 1 ชิ้น\n" +
                        "ผักคอส 1 ต้น\n" +
                        "มะเขือเทศ 1 ลูก\n" +
                        "เกลือ 1 ช้อนชา\n" +
                        "พริกไทยป่น 1 ช้อนชา" ,
                "วิธีทำ \n" +
                        "1.หมักอกไก่ด้วยเกลือและพริกไทยให้ทั่ว แล้วนำไปจี่บนกระทะด้วยไฟอ่อนจนไก่สุกดี\n" +
                        "2.ปิ้งขนมปังมัลติเกรนให้เหลืองกรอบ\n" +
                        "3.หั่นผักคอส มะเขือเทศ อโวคาโด และอกไก่ย่างเตรียมไว้\n" +
                        "4.ประกอบแซนด์วิช โดยการนำผักคอส อกไก่ย่าง มะเขือเทศสไลซ์ และอโวคาโดวางลงบนขนมปังปิ้งตามลำดับ แล้วประกบด้วยขนมปังปิ้งอีกแผ่น\n" +
                        "5.หั่นแซนด์วิชออกเป็นสองชิ้น พร้อมรับประทาน"
            ),
            Menu(
                11,
                R.drawable.lobster,
                "Lobster with cheese",
                125,
                "วัตถุดิบ\n" +
                        "ล็อบสเตอร์ดิบส่วนหาง 5 ตัว\n" +
                        "หอมแขกหั่นเต๋า 1/2 ลูก\n" +
                        "แป้งอเนกประสงค์ 2 ช้อนโต๊ะ\n" +
                        "คุกกิ้งครีม 1 ถ้วยตวง\n" +
                        "น้ำมันมะกอก 1 ช้อนโต๊ะ\n" +
                        "ไวน์ขาว 3 ช้อนโต๊ะ  เนยสด 100 กรัม\n" +
                        "พาร์สลีย์อบแห้ง 1/2 ช้อนโต๊ะ\n" +
                        "เกลือ 1/4 ช้อนชา   พริกไทย 1/4 ช้อนชา\n" +
                        "ชีสพาร์เมซานขูด ตามต้องการ\n" ,
                "วิธีทำ \n" +
                        "1.หมักอกไก่ด้วยเกลือและพริกไทยให้ทั่ว แล้วนำไปจี่บนกระทะด้วยไฟอ่อนจนไก่สุกดี\n" +
                        "2.ปิ้งขนมปังมัลติเกรนให้เหลืองกรอบ\n" +
                        "3.หั่นผักคอส มะเขือเทศ อโวคาโด และอกไก่ย่างเตรียมไว้\n" +
                        "4.ประกอบแซนด์วิช โดยการนำผักคอส อกไก่ย่าง มะเขือเทศสไลซ์ และอโวคาโดวางลงบนขนมปังปิ้งตามลำดับ แล้วประกบด้วยขนมปังปิ้งอีกแผ่น\n" +
                        "5.หั่นแซนด์วิชออกเป็นสองชิ้น พร้อมรับประทาน"
            ),
            Menu(
                12,
                R.drawable.mc,
                "Mac & Cheese",
                500,
                "ส่วนผสม \n" +
                        "มะกะโรนีต้มสุก 250 กรัม\n" +
                        "ชีสผสมชีสกรูแยร์ 100 กรัม\n" +
                        "หอมใหญ่ (หั่นเต๋าละเอียด หรือหั่นแว่น) 2 หัว (ผัดเนยกับเกลือ)\n" +
                        "เบคอน 150 กรัม (ผัดในกระทะพอสุก)\n" +
                        "เกล็ดขนมปัง\n" +
                        "พาร์สลีย์ สำหรับแต่ง" ,
                "วิธีทำ \n" +
                        "1.ตั้งไฟให้ร้อนใส่เนยลงไป เติมแป้งสาลีอเนกประสงค์ ใช้ตะกร้อคนจนแป้งสุก (ถ้าแป้งไม่สุกจะมีกลิ่นแป้ง กินแล้วจะไม่อร่อย)\n" +
                        "2.ค่อย ๆ เติมนมลงไปจนหมด คนให้เข้ากัน เติมลูกจันทน์เทศขูดละเอียดลงไปเล็กน้อย เพื่อให้มีกลิ่นหอม เติมเกลือกับพริกไทยลงไปเล็กน้อย คนจนซอสข้นคล้าย ๆ ดับเบิลครีม\n" +
                        "3.เติมชีสลงไป คนจนส่วนผสมทุกอย่างเป็นเนื้อเดียวกัน"
            ),
            Menu(
                13,
                R.drawable.spaghettimeat,
                "Spaghetti with Meatballs",
                350,
                "ส่วนผสม \n" +
                        "เนื้อบด 500 กรัม   มะเขือเทศ 4 ลูก\n" +
                        "ไข่ 1 ฟอง   หอมใหญ่ 1 ลูก\n" +
                        "เกลือ 1 ช้อนชา   กระเทียม 3 กลีบ\n" +
                        "พริกไทยป่น 1/2 ช้อนชา\n" +
                        "ใบออริกาโนแห้ง \n" +
                        "น้ำเปล่า 3 ช้อนโต๊ะ\n" +
                        "เนย 1 ช้อนโต๊ะ เส้นสปาเกตตี\n" +
                        "น้ำมันมะกอก 1 ช้อนโต๊ะ\n" +
                        "ซอสมะเขือเทศ 3 ช้อนโต๊ะ\n" +
                        "เกลือป่น   พริกไทยป่นเล็กน้อย\n" ,
                "วิธีทำ \n" +
                        "1.ใส่เนื้อบด ปั้นเป็นก้อนกลม เตรียมไว้\n" +
                        "2.ตั้งกระทะ ใส่เนย น้ำมันมะกอกลงไป ใส่เนื้อบดลงทอดให้สุก " +
                        "3.หั่นมะเขือเทศ หัวหอม และกระเทียมเป็นชิ้นเล็ก ๆ เตรียมไว้\n" +
                        "4.ตั้งกระทะ ใส่น้ำมันมะกอกลงลงไป ใส่หอมหัวใหญ่ลงไปผัดให้หอม ตามด้วยกระเทียม ใส่มะเขือเทศลงไปผัดรวมกัน แล้วใส่ซอสมะเขือเทศลงไป ปรุงรสด้วยเกลือและพริกไทย โรยออริกาโน่ คนผสมให้เข้ากัน ใส่มีทบอลลงไป ปิดไฟแล้วคนผสมให้เข้ากัน\n" +
                        "5.ต้มเส้นสปาเกตตี โดยใส่น้ำ น้ำมันมะกอก และเกลือลงในหม้อ นำขึ้นตั้งไฟต้มจนน้ำเดือดแล้วใส่เส้นสปาเกตตีลงไปต้ม หมั่นคนเพื่อไม่ให้เส้นติด ต้มจนเส้นสุก เทลงในกระชอนเพื่อสะเด็ดน้ำจนแห้งแล้วใส่น้ำมันมะกอกลงไปคลุกกับเส้นเพื่อไม่ให้เส้นติดกัน พร้อมเสิร์ฟ"
            )
        )
    }
}
